package day03.locnlg.a;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import day03.locnlg.a.firestore.FireStoreManagement;

public class VerifyCodeActivity extends AppCompatActivity implements View.OnClickListener {
    // cal FireStore
    private FireStoreManagement mFireStore = new FireStoreManagement();

    private FirebaseAuth mAuth;
    private static final String VERIFY_ID = "verifyid";
    private static String getVerifyId;
    private EditText mVerificationField;
    private Button mVerifyButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_code);
        initialView();
    }

    private void initialView(){
        mAuth = FirebaseAuth.getInstance();
        mVerificationField = findViewById(R.id.edit_verification_code);
        mVerifyButton = findViewById(R.id.button_confirm_verification_code);
        mVerifyButton.setOnClickListener(this);
    }

    public static void intentToVerifyCodeActivity(Activity activity,String verifyId){
        Intent intent = new Intent(activity,VerifyCodeActivity.class);
        intent.putExtra(VERIFY_ID,verifyId);
        getVerifyId = intent.getStringExtra(VERIFY_ID);
        activity.startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_confirm_verification_code:
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }
                verifyPhoneNumberWithCode(getVerifyId, code);
                break;
        }
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        // [START verify_with_code]
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        // [END verify_with_code]
        signInWithPhoneAuthCredential(credential);
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            mFireStore.createANewUser(VerifyCodeActivity.this,task.getResult().getUser().getPhoneNumber());
                            // [START_EXCLUDE]
                            // [END_EXCLUDE]
                        } else {
                            // Sign in failed, display a message and update the UI
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                mVerificationField.setError("Invalid code.");
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            // [END_EXCLUDE]
                        }
                    }
                });
    }
}
