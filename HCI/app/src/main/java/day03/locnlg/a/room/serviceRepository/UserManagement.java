package day03.locnlg.a.room.serviceRepository;

import android.app.Activity;
import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.database.DatabaseUtils;
import android.os.AsyncTask;

import day03.locnlg.a.room.dao.UserDAO;
import day03.locnlg.a.room.database.Database;
import day03.locnlg.a.room.model.User;

public class UserManagement {
    private UserDAO mUserDao;

    public UserManagement(Context application) {
        Database db = Database.getDatabase(application);
        this.mUserDao = db.mUserDao();
    }

    public UserManagement(Activity activity) {
        Database db = Database.getDatabase(activity);
        this.mUserDao = db.mUserDao();
    }

    public void deleteAllUser(OnDataCallbackUser mListener){
        DeleteAllUserAsyns deleteAllUserAsyns = new DeleteAllUserAsyns(mUserDao,mListener);
        deleteAllUserAsyns.execute();
    }

    public void addUser(User user, OnDataCallbackUser mListener){
        AddUserAsync addUserAsync = new AddUserAsync(mUserDao, mListener);
        addUserAsync.execute(user);
    }



    public class AddUserAsync extends AsyncTask<User, Void, Void> {
        private UserDAO mUserDao;
        private OnDataCallbackUser mListener;
        public AddUserAsync(UserDAO mUserDao, OnDataCallbackUser mListener) {
            this.mUserDao = mUserDao;
            this.mListener = mListener;
        }

        @Override
        protected Void doInBackground(User... users) {
            mUserDao.addUser(users);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mListener.onSuccess(null);
        }
    }

    public class DeleteAllUserAsyns extends AsyncTask<Void, Void, Void>{
        private UserDAO mUserDao;
        private OnDataCallbackUser mListener;
        public DeleteAllUserAsyns(UserDAO mUserDao, OnDataCallbackUser mListener) {
            this.mUserDao = mUserDao;
            this.mListener = mListener;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mUserDao.deleteAllUser();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            mListener.onSuccess(null);
        }
    }
    public void getAllUser(OnDataCallbackUser listener){
        GetAllUser getAllUserAsync = new GetAllUser(mUserDao, listener);
        getAllUserAsync.execute();
    }
    public class GetAllUser extends AsyncTask<User, Void, Void>{
        private UserDAO mUserDao;
        private User mUser;
        private OnDataCallbackUser mListener;
        public GetAllUser(UserDAO mUserDao, OnDataCallbackUser mListener) {
            this.mUserDao = mUserDao;
            this.mListener = mListener;

        }

        @Override
        protected Void doInBackground(User... users) {
            mUser = mUserDao.getAllUser();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(mUser != null){
                mListener.onSuccess(mUser);
            }else{
                mListener.onDataFail();
            }

        }
    }


    public interface OnDataCallbackUser{
        void onSuccess(User user);
        void onDataFail();
    }
}
