package day03.locnlg.a.room.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "users")
public class User {
    @PrimaryKey(autoGenerate = true)
    private int UserId;

    @ColumnInfo(name = "cloudId")
    private String UserCloudId;

    @ColumnInfo(name = "userName")
    private String UserName;

    @ColumnInfo(name = "userEmail")
    private String UserEmail;

    @ColumnInfo(name = "userPhone")
    private String UserPhone;

    public User() {
    }

    public User(String userCloudId, String userName, String userEmail, String userPhone) {
        UserCloudId = userCloudId;
        UserName = userName;
        UserEmail = userEmail;
        UserPhone = userPhone;
    }

    public User(int userId, String userCloudId, String userName, String userEmail, String userPhone) {
        UserId = userId;
        UserCloudId = userCloudId;
        UserName = userName;
        UserEmail = userEmail;
        UserPhone = userPhone;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getUserCloudId() {
        return UserCloudId;
    }

    public void setUserCloudId(String userCloudId) {
        UserCloudId = userCloudId;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserEmail() {
        return UserEmail;
    }

    public void setUserEmail(String userEmail) {
        UserEmail = userEmail;
    }

    public String getUserPhone() {
        return UserPhone;
    }

    public void setUserPhone(String userPhone) {
        UserPhone = userPhone;
    }
}
