package day03.locnlg.a.room.dao;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import day03.locnlg.a.room.model.User;

@Dao
public interface UserDAO {
    @Query("select * from users")
    LiveData<User> getUserInfo();
    @Query("select * from users")
    User getAllUser();

    @Query("select * from users where cloudId =:aaaa")
    LiveData<User> getBycloudId(String... aaaa);
    @Insert
    void addUser(User... users);

    @Query("delete from users")
    void deleteAllUser();

    @Delete
    void deleteUser(User... users);

    @Update
    void updateUser(User... users);
}
