package day03.locnlg.a;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

public class VerificationPhoneActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "VerificationPhone";

    private static final String KEY_VERIFY_IN_PROGRESS = "key_verify_in_progress";

    private FirebaseAuth mAuth;

    private boolean mVerificationInProgress = false;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private EditText mPhoneNumberField;
    private Button mStartButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification_phone);
        initalView();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential phoneAuthCredential) {
                signInWithPhoneAuthCredential(phoneAuthCredential);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.d(TAG,"onVerificationFailed,e");
                if(e instanceof  FirebaseAuthInvalidCredentialsException){
                    mPhoneNumberField.setText("Invalid Phone Number");
                } else if(e instanceof FirebaseTooManyRequestsException){
                    Log.d(TAG,"Too many request");
                }
            }

            @Override
            public void onCodeSent(String s, PhoneAuthProvider.ForceResendingToken forceResendingToken) {
                super.onCodeSent(s, forceResendingToken);
                Log.d(TAG,"Code sent");
                VerifyCodeActivity.intentToVerifyCodeActivity(VerificationPhoneActivity.this,s);
            }
        };
    }

    public static void intentToVerificationPhoneActivity(Activity activity){
        Intent intent = new Intent(activity,VerificationPhoneActivity.class);
        activity.startActivity(intent);
    }

    private void initalView(){
        mPhoneNumberField = findViewById(R.id.edit_phone_number);
        mStartButton = findViewById(R.id.button_get_verification_code);
        mStartButton.setOnClickListener(this);
        mAuth = FirebaseAuth.getInstance();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.button_get_verification_code:
                if (!validatePhoneNumber()) {
                    return;
                }
                String phone = mPhoneNumberField.getText().toString().replaceFirst("0","+84");
                startPhoneNumberVerification(phone);
                break;
        }
    }

    private void startPhoneNumberVerification(String phonenumber){
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phonenumber,    //Phone number to verify
                60,     // Timeout duration
                TimeUnit.SECONDS, // Unit of timeout
                this,   // Activity (for callback binding)
                mCallbacks); //OnVerificationStateChangedCallbacks
        mVerificationInProgress = true;
    }


    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.setError("Invalid phone number.");
            return false;
        }

        return true;
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = task.getResult().getUser();
                            RegisterActivity.intentToRegisterActivity(VerificationPhoneActivity.this);
                            // [START_EXCLUDE]
                            // [END_EXCLUDE]
                        } else {
                            // Sign in failed, display a message and update the UI
                            if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                                // The verification code entered was invalid
                                // [START_EXCLUDE silent]
                                // [END_EXCLUDE]
                            }
                            // [START_EXCLUDE silent]
                            // Update UI
                            // [END_EXCLUDE]
                        }
                    }
                });
    }
}
