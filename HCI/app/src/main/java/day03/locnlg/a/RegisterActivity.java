package day03.locnlg.a;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import day03.locnlg.a.firestore.FireStoreManagement;

public class RegisterActivity extends AppCompatActivity {
    private EditText edtUserName, edtEmail;
    private Button btnConfirm;

    //Intent from VerifyCodeActivity
    private final static String mIntentCloudId = "cloud";
    private final static String mIntentPhone = "phone";
    private static String mPhone;
    private static String mCloudId;

    //call FireStore
    FireStoreManagement mFireStore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        initialView();
        initialData();
    }

    private void initialView(){
        edtUserName = findViewById(R.id.edit_username_register);
        edtEmail = findViewById(R.id.edit_email_register);
        btnConfirm = findViewById(R.id.button_confirm_register);
    }

    private void initialData(){
        mFireStore = new FireStoreManagement();
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mFireStore.updateInfoUser(RegisterActivity.this, mCloudId, edtUserName.getText().toString(),edtEmail.getText().toString(),mPhone);
            }
        });
    }

    public static void intentToRegisterActivity(Activity activity){
        Intent intent = new Intent(activity,RegisterActivity.class);
        activity.startActivity(intent);
    }

    public static void intentToProfileActivity(Activity context, String cloudId, String phoneNumber) {
        Intent intent = new Intent(context, RegisterActivity.class);
        intent.putExtra(mIntentCloudId, cloudId);
        intent.putExtra(mIntentPhone, phoneNumber);
        context.startActivity(intent);
        mCloudId = intent.getStringExtra(mIntentCloudId);
        mPhone = intent.getStringExtra(mIntentPhone);
    }


}
