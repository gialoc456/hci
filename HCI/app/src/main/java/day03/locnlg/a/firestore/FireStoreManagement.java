package day03.locnlg.a.firestore;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreSettings;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import day03.locnlg.a.HomeActivity;
import day03.locnlg.a.RegisterActivity;
import day03.locnlg.a.VerifyCodeActivity;
import day03.locnlg.a.room.model.User;
import day03.locnlg.a.room.serviceRepository.UserManagement;

public class FireStoreManagement {
    //call repository
    private UserManagement mUserManagement;
    private Context mApplication;

    // call firestore
    private FirebaseFirestore mDocRef = FirebaseFirestore.getInstance();

    // keyword for create user to server
    private static final String USERCOLLECTION = "User";
    private static final String USERCLOUDID_KEY = "UserCloudId";
    private static final String USERNAME_KEY = "UserName";
    private static final String USEREMAIL_KEY = "UserEmail";
    private static final String USERPHONE_KEY = "UserPhone";

    // keyword for create event to server

    // keyword for create booking to server

    public void setupFireStore(){
        FirebaseFirestoreSettings settings = new FirebaseFirestoreSettings.Builder().setPersistenceEnabled(true)
                .build();
        mDocRef.setFirestoreSettings(settings);
    }

    public void createANewUser(final Activity context, final String userPhone){
        Map<String, Object> dataToSave = new HashMap<>();
        dataToSave.put(USERCLOUDID_KEY, "");
        dataToSave.put(USERNAME_KEY, "");
        dataToSave.put(USERPHONE_KEY, userPhone);
        dataToSave.put(USEREMAIL_KEY, "");
        mDocRef.collection(USERCOLLECTION).add(dataToSave)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(final DocumentReference documentReference) {
                        DocumentReference db = mDocRef.collection(USERCOLLECTION).document(documentReference.getId());
                        db.update(USERCLOUDID_KEY, documentReference.getId())
                                .addOnSuccessListener(new OnSuccessListener<Void>() {
                                    @Override
                                    public void onSuccess(Void aVoid) {
                                        RegisterActivity.intentToProfileActivity(context, documentReference.getId(), userPhone);
                                    }
                                });
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    public void updateInfoUser(final Activity activity, final String userCloudId, final String userName, final String userEmail, final String userPhone){
        DocumentReference db = mDocRef.collection(USERCOLLECTION).document(userCloudId);
        db.update(USEREMAIL_KEY, userEmail);
        db.update(USERNAME_KEY, userName)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        HomeActivity.intentToHomeActivity(activity);
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {

            }
        });
    }

    public void createANewBooking(Activity context, String storeId){
        Map<String, Object> dataToSave = new HashMap<>();

    }

}
